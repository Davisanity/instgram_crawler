import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
host = ""
port = 25
local_hostname = 'localhost'
sender = 'davidlin12tw@gmail.com'
receivers = ['tclinzw@tsmc.com']

message = MIMEMultipart()
message['From'] = Header("寄件",'utf-8')
message['To'] = Header("Test")
subject = 'Shiba & Corgi'
message['Subject'] = Header(subject,'utf-8')

#正文内容
message.attach(MIMEText('content……', 'plain', 'utf-8'))

# 附件1，傳送當前目錄下的附件
att1 = MIMEText(open('wia627/a.jpg', 'rb').read(), 'base64', 'utf-8')
att1["Content-Type"] = 'application/octet-stream'
# 這裡的filename可以任意寫，寫甚麼名字，信件中就顯示甚麼名字
att1["Content-Disposition"] = 'attachment; filename="a.jpg"'
message.attach(att1)

try:
	smtpObj = smtplib.SMTP('localhost')
	smtpObj.sendmail(sender, receivers, message.as_string())
	print ("Succeed")
except smtplib.SMTPException:
	print ("Error: Failed")